/**
 * 
 */
package com.jsyunsi.run.JFrame;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;



/**
 * @ 陈东
 * @ 2018年7月10日
 * @ 下午5:01:15
 * @ 日常搬砖
 */
public class LoginFrame extends JFrame{

	//2用户名 密码
	JLabel userLabel;					//JLable  存储文字，图形，图形文字
	JLabel pwdLabel;
	JTextField userText;				//单行文本
	JPasswordField pwdText;				//只能输入数字的密码框
	
	//3、登陆和取消按钮
	JButton LoginBtn,CancelBtn;
	String username;
	String userpwd;
		
	//加载音乐
	File file;
	URL url;
	URI uri;
	AudioClip  au;
	
	//构造方法
	public LoginFrame() {

		try {
			file = new File("sound/main.wav");
			uri = file.toURI();
			url = uri.toURL();
			au = Applet.newAudioClip(url);
			au.loop();
		} catch (MalformedURLException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		
		
		//2、用户名 密码
		userLabel = new JLabel("用户名");
		userLabel.setBounds(40,300,150,30);
		pwdLabel = new JLabel("密    码");
		pwdLabel.setBounds(40, 340, 150, 30);
		this.add(pwdLabel);
		this.add(userLabel);
		
		userText = new JTextField();
		userText.setBounds(90, 305, 120, 25);
		//Lowered设置边框凹下去			 Raised:设置边框凸出来
		userText.setBorder(BorderFactory.createLoweredBevelBorder());		
		this.add(userText);
		pwdText = new JPasswordField();
		pwdText.setBounds(90, 345, 120, 25);
		pwdText.setBorder(BorderFactory.createLoweredBevelBorder());
		this.add(pwdText);
		
		
		//3
		LoginBtn = new JButton("登录");
		LoginBtn.setBounds(80, 380, 60, 25);
		//设置按钮前景色
		LoginBtn.setForeground(Color.blue);
		//设置按钮背景色(边框颜色)
		LoginBtn.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		//监听登陆按钮事件
		LoginBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// 点击登陆按钮生成的事件
				username = userText.getText();
				userpwd = pwdText.getText();
				if(username.length() == 0)
				{
					JOptionPane.showMessageDialog(null, "用户名不能为空");
				}else if(username.equals("陈东东") == false) {
					JOptionPane.showMessageDialog(null, "用户名错误");
				}else if(userpwd.equals("123456") == false) {
					JOptionPane.showMessageDialog(null, "密码错误");
				}
				if(username.equals("陈东东") == true && userpwd.equals("123456") == true) {
					//JOptionPane.showMessageDialog(null, "登陆成功！");
					//成功后 1、跳转到下一界面 2、关闭当前界面
					//1、跳转下一界面
					new MenuFrame();
					//2、关闭当前界面
					dispose();
					//3.关闭音乐
					au.stop();
					
				}
			}
		});
		this.add(LoginBtn);
		
		
		
		CancelBtn = new JButton("取消");
		CancelBtn.setBounds(160, 380, 60, 25);
		//设置按钮前景色
		CancelBtn.setForeground(Color.blue);
		//设置按钮背景色(边框颜色)
		CancelBtn.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		//监听取消按钮事件
		CancelBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// 点击登陆按钮生成的事件
				JOptionPane.showMessageDialog(null, "确定退出游戏吗！");
				//关闭音乐
				au.stop();
				dispose(); 			//关闭当前界面
			}
		});
		this.add(CancelBtn);
		
		BackImage back = new BackImage();
		this.add(back);
		this.setSize(1000, 550);
		this.setLocationRelativeTo(null);									//居中
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);				//退出
		this.setUndecorated(true);											//菜单不显示
		this.setIconImage(new ImageIcon("image/115.png").getImage());		//设置窗口LOGO
		this.setVisible(true);
	}
	
	
	
	public static void main(String[] args) {
		
			new LoginFrame();
	}
	//背景图片面板
	class BackImage extends JPanel{
		Image background;
		
		public BackImage() {
			try {
				background = ImageIO.read(new File("image/login.jpg"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		//绘制背景图片的方法
		public void paint(Graphics g) {
			// TODO 自动生成的方法存根
			super.paint(g);
			g.drawImage(background, 0, 0,1000, 550, null);
		}
		
	}
}















